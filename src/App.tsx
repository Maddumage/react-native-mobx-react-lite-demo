import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, StatusBar} from 'react-native';
import Button from './components/Button/Button';
import CountView from './components/CountView/CountView';
import StoreProvider from './store/context';
import UserView from './components/UserView/UserView';

const App: React.FC = () => {
  return (
    <StoreProvider>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.textTitle}>mobx-react-lite-demo</Text>
        </View>
        <CountView />
        <Button type={'ADD'}></Button>
        <Button type={'MINUS'}></Button>
        <UserView />
      </SafeAreaView>
    </StoreProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  textTitle: {
    color: 'green',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default App;
