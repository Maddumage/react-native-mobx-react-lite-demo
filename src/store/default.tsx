import React from 'react';
import {TStore} from './store'

export const storeContext = React.createContext<TStore | null>(null);
