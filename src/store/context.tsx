import React from 'react';
import {useLocalStore} from 'mobx-react-lite';
import {createStore} from './store';
import {storeContext} from './default';

export const StoreProvider: React.FC = ({children}) => {
  const store = useLocalStore(createStore);
  return (
    <storeContext.Provider value={store}>{children}</storeContext.Provider>
  );
};

export default StoreProvider;
