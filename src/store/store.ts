interface userDTO {
  name: string;
  address: string;
}

export const userStore: userDTO[] = [
  {name: 'User one', address: 'Address one'},
  {name: 'User two', address: 'Address two'},
];

export const createStore = () => {
  const store = {
    company: 'unicorn',
    count: 1,
    get allUsers() {
      return userStore;
    },
    get getCount() {
      return this.count;
    },
    get getNoOfUsers() {
      return userStore.length;
    },
  };
  return store;
};

export type TStore = ReturnType<typeof createStore>;
