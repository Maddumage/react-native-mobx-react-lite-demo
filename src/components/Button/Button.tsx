import React, {Children} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useObserver} from 'mobx-react-lite';
import {storeContext} from '../../store/default';

export const Button = ({type = 'ADD', text = '+1'}) => {
  const store = React.useContext(storeContext);
  if (!store) {
    throw Error;
  }
  return useObserver(() => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: type === 'ADD' ? 'green' : 'red',
          width: 100,
          height: 48,
          borderRadius: 5,
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          margin: 16,
        }}
        onPress={() => {
          if (type === 'ADD') {
            store.count++;
          } else {
            store.count--;
          }
        }}>
        <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
          {type === 'ADD' ? '+1' : '-1'}
        </Text>
      </TouchableOpacity>
    );
  });
};

export default Button;
