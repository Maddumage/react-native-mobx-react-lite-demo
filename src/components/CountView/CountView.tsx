import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useObserver} from 'mobx-react-lite';
import {storeContext} from '../../store/default';

export const CountView = () => {
  const store = React.useContext(storeContext);
  if (!store) {
    throw Error;
  }
  return useObserver(() => {
    return (
      <View
        style={{
          backgroundColor: 'gray',
          width: 100,
          height: 100,
          borderRadius: 5,
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          margin:16
        }}>
        <Text style={{color: '#fff', fontSize: 44, fontWeight: 'bold'}}>
          {store.count}
        </Text>
      </View>
    );
  });
};

export default CountView;
