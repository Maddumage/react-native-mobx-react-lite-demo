import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import userDTO from './userDTO';
import {rootData} from '../../store/hook';
import {storeContext} from '../../store/default';
import Button from '../Button/Button';

//export const storeContext = React.createContext<TStore|null>(null);
//const storeContext = React.createContext<TStore | null | userDTO[]>(users);

export const UserView: React.FC<userDTO> = () => {
  let {name, count} = rootData(store => ({
    name: store.company,
    count: store.getCount,
  }));
  const store = React.useContext(storeContext);
  if (!store) throw 'error';
  return (
    <View style={{alignSelf: 'center'}}>
      <Text style={{fontSize: 16}}>
        name is {name} and {count} users
      </Text>
      <Button />
    </View>
  );
};

export default UserView;
